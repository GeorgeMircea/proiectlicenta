For opening the project, install these:

nodeJS: https://nodejs.org/en/download/

CMake: https://cmake.org/download/

ez-gen: https://github.com/hamez0r/ez-gen
In terminal, go to ez-gen folder, and execute:
npm install -g

In terminal, go in any folder from this repo, then execute:
ez-gen

This will generate a Visual Studio solution for you.


UML diagrams:
For opening the UML diagrams, all you have to do is to acces https://www.draw.io/ , then press "Open diagram" and choose the ProiectLicenta-UMLDiagram.xml file from this repo.

Use case diagrams:
For opening the use case diagrams, all you have to do is to acces https://www.draw.io/ , then press "Open diagram" and choose the ProiectLicenta-UseCaseDiagram.xml file from this repo.
#pragma once
#include<Windows.h>
#include<memory>
#include<iostream>
#include<random>
#include<string>

#include"Interfaces\IGame.h"
#include"Interfaces\IPlayer.h"
#include"Interfaces\ILoader.h"


//this class is sloads the dll which represents the played game
//and dlls which represent the players
class Server
{

	IGame* m_playedGame;
	HINSTANCE dllHandle;

	std::string m_player1;
	std::string m_player2;

	ILoader* m_loader;

	//number of wins
	int m_player1NumberOfWins = 0;
	int m_player2NumberOfWins = 0;
	int m_numberOfDraws = 0;

	int m_seconds=0;

	//the list of all players
	ArrayOfNPlayers m_playersList;

	int m_numberOfPlayers = 0;

	//number of games you wish to play
	int m_maxNumberOfGames = 1;

	//contains the complete path to the folder containing the executable
	std::string m_exeFolder;

	//select 2 random players from the playersList
	void selectPlayers();

	//starts the selected game with the selected players
	void start2PlayersGame();

	//displays the winner
	void displayWinner();

	//sends the message to all players
	void sendToAll(const std::string& message);


public:
	Server();
	~Server() = default;

	//load the chosen loader
	void setLoader(std::string loaderName);

	//loads the game at the gamePath location
	//the game has to implement IGame interface ("..\Interfaces\IGame.h")
	void loadGame(std::string gamePath);

	//finds all players from playersFolder into playersList member
	//every player has to implement IPlayer interface ("..\Interfaces\IPlayer.h")
	void findAllPlayers(std::string playersFolder="Players");

	//loads a single player
	void loadPlayer(const std::string& playerPath, const std::string& playerName);

	//selects the players and starts the game
	void startGame();

	//sets the max number of games to be played
	void setNumberOfGames(int numberOfGames);

	//show the statistics
	void showStatistics() const;

	//sets the delay
	void setDelay(int seconds);
};


#include "Server.h"

void Server::selectPlayers()
{
	std::random_device rd;
	std::mt19937 gen(rd());
	std::uniform_int_distribution<> dis(0, m_numberOfPlayers - 1);

	int firstChoice = dis(gen);

	int secondChoice;
	do
	{
		secondChoice = dis(gen);
	} while (firstChoice == secondChoice);

	m_player1 = m_playersList[firstChoice];
	m_player2 = m_playersList[secondChoice];

	loadPlayer(m_player1, "Player1");
	loadPlayer(m_player2, "Player2");

}

void Server::displayWinner()
{
	Winner winner = m_playedGame->getWinner();
	switch (winner)
	{
	case Winner::PLAYER_ONE:
		std::cout << m_player1 << " WON!" << std::endl;
		m_loader->sendMessageToPlayer("You win!", "Player1");
		m_loader->sendMessageToPlayer("You lose!", "Player2");
		++m_player1NumberOfWins;
		return;
	case Winner::PLAYER_TWO:
		std::cout << m_player2 << " WON!" << std::endl;
		m_loader->sendMessageToPlayer("You lose!", "Player1");
		m_loader->sendMessageToPlayer("You win!", "Player2");
		++m_player2NumberOfWins;
		return;
	case Winner::DRAW:
		std::cout << "DRAW!" << std::endl;
		m_loader->sendMessageToPlayer("It's a draw!", "Player1");
		m_loader->sendMessageToPlayer("It's a draw!", "Player2");
		++m_numberOfDraws;
		return;
	default:
		std::cout << "Error at deciding the winner!";
		return;
	}
}

void Server::sendToAll(const std::string & message)
{
	for (auto i = 0; i < 2; ++i)
	{
		m_loader->sendMessageToPlayer(message, "Player" + std::to_string(i + 1));
	}
}

Server::Server()
{
	char buffer[MAX_PATH];
	GetModuleFileName(NULL, buffer, MAX_PATH);
	std::string::size_type pos = std::string(buffer).find_last_of("\\/");
	m_exeFolder = std::string(buffer).substr(0, pos);
}

void Server::setLoader(std::string loaderName)
{
	typedef ILoader*(CALLBACK* FUNCTYPE)();
	FUNCTYPE  pLoader = NULL;

	//Load the dll and keep the handle to it
	loaderName += ".dll";

	std::string loaderPath = "Loaders\\" + loaderName;


	TCHAR* gameNameTCHAR = new TCHAR[loaderPath.length() + 1];
	std::copy(loaderPath.begin(), loaderPath.end(), gameNameTCHAR);
	gameNameTCHAR[loaderPath.length()] = '\0';
	dllHandle = LoadLibrary(gameNameTCHAR);

	// If the handle is valid, try to get the function address. 
	if (dllHandle != nullptr)
	{
		//Get pointer to our function using GetProcAddress:
		pLoader = (FUNCTYPE)GetProcAddress(dllHandle, "getLoader");

		// If the function address is valid, call the function. 
		if (pLoader != nullptr)
		{
			m_loader = pLoader();
		}
		else
		{
			std::cout << "getLoader() method doesn't exist in your dll!" << std::endl;
		}
	}
	else
	{
		std::cout << "Invalid dll!" << std::endl;
	}

}

void Server::loadGame(std::string gameName)
{
	typedef IGame*(CALLBACK* FUNCTYPE)();
	FUNCTYPE  pGame = NULL;

	//Load the dll and keep the handle to it
	gameName += ".dll";

	std::string gamePath = "Games\\" + gameName;

	TCHAR* gameNameTCHAR = new TCHAR[gamePath.length() + 1];
	std::copy(gamePath.begin(), gamePath.end(), gameNameTCHAR);
	gameNameTCHAR[gamePath.length()] = '\0';
	dllHandle = LoadLibrary(gameNameTCHAR);

	// If the handle is valid, try to get the function address. 
	if (dllHandle != nullptr)
	{
		//Get pointer to our function using GetProcAddress:
		pGame = (FUNCTYPE)GetProcAddress(dllHandle, "getGame");

		// If the function address is valid, call the function. 
		if (pGame != nullptr)
		{
			m_playedGame = pGame();
		}
		else
		{
			std::cout << "getGame() method doesn't exist in your dll!" << std::endl;
		}

		std::cout << m_playedGame->getGameName() << " loaded!" << std::endl;
	}
	else
	{
		std::cout << "Invalid dll!" << std::endl;
	}

}

void Server::findAllPlayers(std::string playersFolder)
{

	playersFolder = m_exeFolder + "\\" + playersFolder + "\\";
	m_playersList = m_loader->findAllPlayers(playersFolder, m_playedGame->getGameName());
	m_numberOfPlayers = m_loader->getNumberOfPlayers();
}

void Server::start2PlayersGame()
{
	//for the posibility to play again against the same player
	bool restart;
	auto gameIndex = 0;
	bool exit = false;

	do
	{
		restart = true;
		std::string messageFromPlayer;
		std::string messageFromServer;



		while (!m_playedGame->gameEnded())
		{
			//tell player1 to send a message
			//tell player2 to wait
			m_loader->sendMessageToPlayer("Your turn", "Player1");
			m_loader->sendMessageToPlayer("Wait for the other player's move", "Player2");

			Sleep(m_seconds);

			//move from player1
			messageFromPlayer = m_loader->receiveMessageFromPlayer("Player1");
			
			if (messageFromPlayer == "/error")
			{
				exit = true;
				break;
			}

			while (!m_playedGame->setMessage(messageFromPlayer + " Player1"))
			{
				m_loader->sendMessageToPlayer("Invalid message. Please try again!", "Player1");
				messageFromPlayer = m_loader->receiveMessageFromPlayer("Player1");
				if (messageFromPlayer == "/error")
				{
					exit = true;
					break;
				}
			}

			if (exit)
				break;


			messageFromServer = m_playedGame->getMessage();
			sendToAll("@" + messageFromServer);
			
			Sleep(m_seconds);

			if (!m_playedGame->gameEnded())
			{
				//tell player1 to wait
				//tell player2 to send a message
				m_loader->sendMessageToPlayer("Wait for the other player's move", "Player1");
				m_loader->sendMessageToPlayer("Your turn", "Player2");

				messageFromPlayer = m_loader->receiveMessageFromPlayer("Player2");

				if (messageFromPlayer == "/error")
				{
					exit = true;
					break;
				}

				while (!m_playedGame->setMessage(messageFromPlayer + " Player2"))
				{
					m_loader->sendMessageToPlayer("Invalid message. Please try again!", "Player2");
					messageFromPlayer = m_loader->receiveMessageFromPlayer("Player2");
					if (messageFromPlayer == "/error")
					{
						exit = true;
						break;
					}
				}

				if (exit)
					break;

				messageFromServer = m_playedGame->getMessage();

				sendToAll("@" + messageFromServer);

				Sleep(m_seconds);

			}
		}
		Sleep(m_seconds);
		displayWinner();
		Sleep(m_seconds);

		if (exit)
			break;

		//ask players if they wish to play again
		messageFromServer = "Play again? (y/n)";
		for (auto i = 0; i < 2; ++i)
		{
			std::string player = "Player" + std::to_string(i + 1);
			m_loader->sendMessageToPlayer(messageFromServer, player);

			messageFromPlayer = m_loader->receiveMessageFromPlayer(player);
			if (messageFromPlayer == "/error")
			{
				exit = true;
				break;
			}

			//if anyone doesn't want to play again this game ends
			if ("n" == messageFromPlayer || "N" == messageFromPlayer)
			{
				restart = false;
				sendToAll("Game Over!");
			}
		}

		if (exit)
			break;

		m_playedGame->restart();
		++gameIndex;
	} while (restart || gameIndex < m_maxNumberOfGames);
}

void Server::loadPlayer(const std::string& playerPath, const std::string& playerName)
{
	m_loader->loadPlayer(playerPath, playerName);
}

void Server::startGame()
{
	selectPlayers();
	start2PlayersGame();
}

void Server::setNumberOfGames(int numberOfGames)
{
	m_maxNumberOfGames = numberOfGames;
}

void Server::showStatistics() const
{
	std::cout << std::endl;
	std::cout << m_player1 << " " << m_player1NumberOfWins << " wins" << std::endl;
	std::cout << m_player2 << " " << m_player2NumberOfWins << " wins" << std::endl;
	std::cout << "Draws " << m_numberOfDraws << std::endl;
}

void Server::setDelay(int seconds)
{
	m_seconds = seconds;
}

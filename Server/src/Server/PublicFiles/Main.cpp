#include"Server.h"
#include<sstream>
#include<tinyxml.h>

#define chat 1

#define tcp 1
#define dll !tcp

#define paper 0
#define five !paper

int main()
{
//
//#if chat
//	server->setLoader("TcpIpLoader");
//	server->loadGame("Chat");
//	server->findAllPlayers();
//	server->startGame();
//	
//
//#else
//#if tcp
//	/**************************TcpIpLoader********************************************************/
//
//	server->setLoader("TcpIpLoader");
//	server->setDelay(20);
//#if paper
//	/*************PaperRockScissors*************/
//
//	server->loadGame("PaperRockScissors");
//
//#else
//	server->loadGame("FiveInLine");
//
//#endif
//	server->findAllPlayers();
//	server->startGame();
//
//#else
//	/**************************DllLoader********************************************************/
//
//	server->setLoader("DllLoader");
//#if paper
//	/*************PaperRockScissors*************/
//
//	server->loadGame("PaperRockScissors");
//	server->setNumberOfGames(100);
//
//#else
//	/****************FiveInLine****************/
//
//	server->loadGame("FiveInLine");
//	server->setNumberOfGames(100);
//
//#endif
//	server->findAllPlayers();
//	server->startGame();
//
//#endif
//	server->showStatistics();
//
//#endif


	//create the server
	std::unique_ptr<Server> server = std::make_unique<Server>();


	//start reading from xml
	TiXmlDocument doc("E:\\Siemens\\ProiectLicenta\\Server\\build_win32\\bin\\config.xml");
	if (!doc.LoadFile())
	{
		std::cout << doc.ErrorDesc() << std::endl;
		return 1;
	}

	//XML body
	TiXmlElement* body = doc.FirstChildElement();
	if (body == NULL)
	{
		std::cout << "Failed to load file: No body element."
			<< std::endl;
		doc.Clear();
		return 1;
	}

	//get all children
	for (TiXmlElement* elem = body->FirstChildElement(); elem != NULL; elem = elem->NextSiblingElement())
	{
		std::string elemName = elem->Value();
		std::string attr;

		//get the game
		if (elemName == "Game")
		{
			attr = elem->Attribute("gameName");
			if (attr != "")
				server->loadGame(attr);
		}

		//get the loader
		else if (elemName == "Loader")
		{
			attr = elem->Attribute("loaderName");
			if (attr != "")
			{
				server->setLoader(attr);

				//if the loader is "TcpIpLoader" set the delay 20ms
				if (attr == "TcpIpLoader")
					server->setDelay(20);
			}
		}

		//get the number of games
		else if (elemName == "NumberOfGames")
		{
			attr = elem->Attribute("numberOfGames");
			if (attr != "")
			{
				int numberOfGames = std::stoi(attr);
				server->setNumberOfGames(numberOfGames);
			}
		}
	}
	doc.Clear();

	server->findAllPlayers();
	server->startGame();
	server->showStatistics();

	system("PAUSE");
	return 0;
}
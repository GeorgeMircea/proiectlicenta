#pragma once
#include<string>

class IPlayer
{
public:
	IPlayer() = default;
	virtual ~IPlayer() = default;

	//returns the move made by the player
	//the move is considered a string
	virtual std::string sendMessage() = 0;

	//sets the message receved by the player from the game 
	virtual void receiveMessage(const std::string& message) = 0;

	//sets the name of the player
	virtual void setName(const std::string& name) = 0;

	//gets the name of the player
	virtual std::string getName() = 0;
};

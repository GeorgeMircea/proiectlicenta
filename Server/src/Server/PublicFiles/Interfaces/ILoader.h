#pragma once
#include<string>
#include<array>
#include"IPlayer.h"
const int MAX_NUMBER_OF_PLAYERS = 50;
typedef std::array<std::string, MAX_NUMBER_OF_PLAYERS> ArrayOfNPlayers;
class ILoader
{
public:
	ILoader() = default;
	virtual ~ILoader() = default;

	//returns a list of players which can connect to the server
	//or can be loaded to the server
	virtual ArrayOfNPlayers  findAllPlayers(const std::string& playersFolder, const std::string& gameName) = 0;

	//load a specific player
	virtual void loadPlayer(const std::string& playerFile, const std::string& playerName) = 0;

	//returns  the actual number of players
	virtual int getNumberOfPlayers() = 0;

	//send message to playerName player
	virtual void sendMessageToPlayer(const std::string& message, const std::string& playerName) = 0;

	//receive message from playerName player
	virtual std::string receiveMessageFromPlayer(const std::string& playerName) = 0;
};
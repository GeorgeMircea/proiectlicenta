#include "Player.h"

Player::Player()
{
	m_message = "";  m_name = "";
}

std::string Player::sendMessage()
{
	if ("n" == m_message)
	{
		m_message = "";
		return "n";
	}
	strategy();
	return m_message;
}

void Player::receiveMessage(const std::string& message)
{
	if ("Play again? (y/n)" == message)
	{
		m_message = "n";
		return;
	}
	if (message[0]=='@')
	{
		m_message = message.substr(1);
		interpretMessage(message.substr(1));
	}
}

void Player::setName(const std::string& name)
{
	m_name = name;
}

std::string Player::getName()
{
	return m_name;
}


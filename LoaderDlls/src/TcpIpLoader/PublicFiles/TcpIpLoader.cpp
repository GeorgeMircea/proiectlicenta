#include "TcpIpLoader.h"

TcpIpLoader::TcpIpLoader()
{
	if (initialize() != 0)
		std::cout << "Error at initializing the connection!" << std::endl;
}

int TcpIpLoader::initialize()
{
	// Initialize Winsock
	iResult = WSAStartup(MAKEWORD(2, 2), &wsaData);
	if (iResult != 0) {
		std::cout << "WSAStartup failed with error: " << iResult << std::endl;
		return 1;
	}

	ZeroMemory(&hints, sizeof(hints));
	hints.ai_family = AF_INET;
	hints.ai_socktype = SOCK_STREAM;
	hints.ai_protocol = IPPROTO_TCP;
	hints.ai_flags = AI_PASSIVE;

	// Resolve the server address and port
	iResult = getaddrinfo(NULL, DEFAULT_PORT, &hints, &result);
	if (iResult != 0)
	{
		std::cout << "getaddrinfo failed with error: " << iResult << std::endl;
		WSACleanup();
		return 1;
	}

	// Create a SOCKET for connecting to server
	ListenSocket = socket(result->ai_family, result->ai_socktype, result->ai_protocol);
	if (ListenSocket == INVALID_SOCKET) {
		std::cout << "socket failed with error: " << WSAGetLastError() << std::endl;
		freeaddrinfo(result);
		WSACleanup();
		return 1;
	}

	// Setup the TCP listening socket
	iResult = bind(ListenSocket, result->ai_addr, (int)result->ai_addrlen);
	if (iResult == SOCKET_ERROR) {
		std::cout << "bind failed with error: " << WSAGetLastError() << std::endl;
		freeaddrinfo(result);
		closesocket(ListenSocket);
		WSACleanup();
		return 1;
	}

	freeaddrinfo(result);

	iResult = listen(ListenSocket, SOMAXCONN);
	if (iResult == SOCKET_ERROR) {
		std::cout << "listen failed with error: " << WSAGetLastError() << std::endl;
		closesocket(ListenSocket);
		WSACleanup();
		return 1;
	}

	return 0;
}

ILoader & TcpIpLoader::getInstance()
{
	static TcpIpLoader instance;
	return instance;
}

TcpIpLoader::~TcpIpLoader()
{
	closesocket(ListenSocket);
	for (int i = 0; i < MAX_CONNECTIONS; i++)
		closesocket(ClientSocket[i]);

	closesocket(m_player1Socket);
	closesocket(m_player2Socket);

	freeaddrinfo(result);

	WSACleanup();
}

ArrayOfNPlayers TcpIpLoader::findAllPlayers(const std::string& playersFolder, const std::string& gameName)
{
	char* recvbuf;
	int recvbuflen = DEFAULT_BUFLEN;

	SOCKET tempConnection = INVALID_SOCKET;
	int num = 0;

	//num will be used for limiting the number of players at 2
	while (num < 2 && (tempConnection = accept(ListenSocket, nullptr, nullptr)) != INVALID_SOCKET)
	{
		recvbuf = new char[DEFAULT_BUFLEN];
		memset(&recvbuf[0], '\0', DEFAULT_BUFLEN);

		ClientSocket[m_numberOfPlayers] = tempConnection;
		iResult = recv(ClientSocket[m_numberOfPlayers], recvbuf, recvbuflen, 0);
		if (iResult > 0)
		{
			std::string playerName;
			playerName.assign(recvbuf, iResult);
			std::cout << playerName << " wants to connect!" << std::endl;
			m_playerNames[m_numberOfPlayers++] = playerName;
			num++;
		}
	}

	delete[] recvbuf;
	return m_playerNames;
}

void TcpIpLoader::loadPlayer(const std::string& playerPath, const std::string& playerName)
{
	if (playerName == "Player1")
	{
		int position = std::distance(m_playerNames.begin(), std::find(m_playerNames.begin(), m_playerNames.end(), playerPath));
		m_player1Socket = ClientSocket[position];
	}
	else if (playerName == "Player2")
	{
		int position = std::distance(m_playerNames.begin(), std::find(m_playerNames.begin(), m_playerNames.end(), playerPath));
		m_player2Socket = ClientSocket[position];
	}
	else
		std::cout << "Error at loading player!" << std::endl;
}

int TcpIpLoader::getNumberOfPlayers()
{
	return m_numberOfPlayers;
}

void TcpIpLoader::sendMessageToPlayer(const std::string& message, const std::string& playerName)
{
	SOCKET currentSocket = INVALID_SOCKET;
	if (playerName == "Player1")
		currentSocket = m_player1Socket;
	if (playerName == "Player2")
		currentSocket = m_player2Socket;


	char *sendbuf = new char[message.length() + 1];
	strcpy(sendbuf, message.c_str());

	iResult = send(currentSocket, sendbuf, (int)strlen(sendbuf), 0);
	if (iResult == SOCKET_ERROR)
	{
		//std::cout << playerName + " disconnected\n";
		closesocket(currentSocket);
		WSACleanup();
		delete[] sendbuf;
		return;
	}
	delete[] sendbuf;
}

std::string TcpIpLoader::receiveMessageFromPlayer(const std::string& playerName)
{
	char* recvbuf;
	int recvbuflen = DEFAULT_BUFLEN;

	SOCKET currentSocket = INVALID_SOCKET;
	std::string resultedMessage = "";
	if (playerName == "Player1")
		currentSocket = m_player1Socket;
	if (playerName == "Player2")
		currentSocket = m_player2Socket;

	recvbuf = new char[DEFAULT_BUFLEN];
	memset(&recvbuf[0], '\0', DEFAULT_BUFLEN);
	auto start = std::chrono::system_clock::now();
	auto end = start;
	do
	{
		iResult = recv(currentSocket, recvbuf, recvbuflen, 0);
		if (iResult > 0)
		{
			printf("Bytes received: %d\n", iResult);
			resultedMessage.assign(recvbuf, iResult);
			std::cout << resultedMessage << std::endl;

		}
		else if (iResult == 0)
			printf("Connection closing...\n");
		else
		{
			std::cout << playerName + " disconnected\n";
			closesocket(currentSocket);
			WSACleanup();
			resultedMessage = "/error";
			break;
		}
		end = std::chrono::system_clock::now();
	} while (iResult <= 0 && (end - start).count() <= 5.0);

	return resultedMessage;
}

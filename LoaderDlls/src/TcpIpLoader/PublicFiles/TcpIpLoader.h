#pragma once
#include"Headers.h"

#define WIN32_LEAN_AND_MEAN

#include <windows.h>
#include <ws2tcpip.h>
#include<iostream>

#include<chrono>

#include <string>

// Need to link with Ws2_32.lib
#pragma comment (lib, "Ws2_32.lib")

#define DEFAULT_BUFLEN 512
#define DEFAULT_PORT "27015"

class TcpIpLoader :
	public ILoader
{
private:

	static const int MAX_CONNECTIONS = 50;
	WSADATA wsaData;
	int iResult;

	SOCKET ListenSocket = INVALID_SOCKET;
	SOCKET ClientSocket[MAX_CONNECTIONS];

	struct addrinfo *result = NULL;
	struct addrinfo hints;


	ArrayOfNPlayers m_playerNames;

	int m_numberOfPlayers = 0;
	TcpIpLoader();

	SOCKET m_player1Socket = INVALID_SOCKET;
	SOCKET m_player2Socket = INVALID_SOCKET;

	int initialize();

public:
	static ILoader& getInstance();
	virtual ~TcpIpLoader();

	// Inherited via ILoader
	//finds all players
	virtual ArrayOfNPlayers findAllPlayers(const std::string& playersFolder, const std::string& gameName) override;
	
	//establises the connection with the player path
	virtual void loadPlayer(const std::string& playerPath, const std::string& playerName);
	
	//returns number of players
	virtual int getNumberOfPlayers();

	// Inherited via ILoader
	//sends message to playerName
	virtual void sendMessageToPlayer(const std::string& message, const std::string& playerName) override;
	
	//receives message from playerName
	virtual std::string receiveMessageFromPlayer(const std::string& playerName) override;
};


extern "C"
{
	__declspec(dllexport) ILoader* getLoader();
};


//create a PaperRockScissors instance
ILoader* getLoader()
{
	return &TcpIpLoader::getInstance();
}

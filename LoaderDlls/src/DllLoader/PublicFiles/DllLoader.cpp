#include "DllLoader.h"



ILoader & DllLoader::getInstance()
{
	static DllLoader instance;
	return instance;
}

ArrayOfNPlayers DllLoader::findAllPlayers(const std::string& playersFolder, const std::string& gameName)
{
	ArrayOfNPlayers playersList;

	WIN32_FIND_DATA FindFileData;
	std::string img = playersFolder + gameName + "_*.dll";

	TCHAR* file = new TCHAR[img.length() + 1];
	std::copy(img.begin(), img.end(), file);
	file[img.length()] = '\0';

	HANDLE hFind = FindFirstFile(file, &FindFileData);


	if (hFind == INVALID_HANDLE_VALUE)
	{
		return ArrayOfNPlayers();
	}
	else
	{
		do
		{
			//std::wstring fileName(FindFileData.cFileName);
			//playersList[m_numberOfPlayers++] = std::string(fileName.begin(), fileName.end());
			playersList[m_numberOfPlayers++] = std::string(FindFileData.cFileName);
			//std::cout << m_playersList[m_playerIndex - 1] << std::endl;

		} while (FindNextFile(hFind, &FindFileData));

	}
	FindClose(hFind);
	return playersList;
}

void DllLoader::loadPlayer(const std::string& playerFile, const std::string& playerName)
{
	typedef IPlayer*(CALLBACK* FUNCTYPE)();
	FUNCTYPE  player = NULL;

	std::string playerPath = "Players\\" + playerFile;

	TCHAR* gameNameTCHAR = new TCHAR[playerPath.length() + 1];
	std::copy(playerPath.begin(), playerPath.end(), gameNameTCHAR);
	gameNameTCHAR[playerPath.length()] = '\0';
	HINSTANCE dllHandle = LoadLibrary(gameNameTCHAR);

	// If the handle is valid, try to get the function address. 
	if (dllHandle != nullptr)
	{
		//Get pointer to our function using GetProcAddress:
		player = (FUNCTYPE)GetProcAddress(dllHandle, "getPlayer");

		// If the function address is valid, call the function. 
		if (player != nullptr)
		{
			if (playerName == "Player1")
				m_player1 = player();
			if (playerName == "Player2")
				m_player2 = player();
		}
		else
		{
			return;
		}

	}
	else
	{
		return;
	}

}

int DllLoader::getNumberOfPlayers()
{
	return m_numberOfPlayers;
}

void DllLoader::sendMessageToPlayer(const std::string& message, const std::string& playerName)
{
	if (playerName == "Player1")
		m_player1->receiveMessage(message);
	if (playerName == "Player2")
		m_player2->receiveMessage(message);

}

std::string DllLoader::receiveMessageFromPlayer(const std::string& playerName)
{
	if (playerName == "Player1")
		return m_player1->sendMessage();
	if (playerName == "Player2")
		return m_player2->sendMessage();
	return std::string();
}

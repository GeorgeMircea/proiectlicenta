#pragma once
#include"Headers.h"
#include<Windows.h>
#include<array>

#include<iostream>
#include<experimental\filesystem>


class DllLoader :
	public ILoader
{
	int m_numberOfPlayers = 0;


	IPlayer* m_player1;
	IPlayer* m_player2;

public:
	DllLoader() = default;
	static ILoader & getInstance();
	virtual ~DllLoader() = default;

	//returns a string formed by all players path from playersFolder
	virtual ArrayOfNPlayers findAllPlayers(const std::string& playersFolder, const std::string& gameName) override;

	virtual int getNumberOfPlayers() override;

	//sets message to playerName player
	virtual void sendMessageToPlayer(const std::string& message, const std::string& playerName) override;
	
	//returns message from playerName
	virtual std::string receiveMessageFromPlayer(const std::string& playerName) override;

	//load a specific player
	virtual void loadPlayer(const std::string& playerFile, const std::string& playerName) override;
};


extern "C"
{
	__declspec(dllexport) ILoader* getLoader();
};


//create a PaperRockScissors instance
ILoader* getLoader()
{
	return &DllLoader::getInstance();
}

\select@language {romanian}
\contentsline {chapter}{\numberline {1}Introducere}{1}{chapter.1}
\contentsline {section}{\numberline {1.1}Context general}{1}{section.1.1}
\contentsline {section}{\numberline {1.2}Ideea și scopul lucrării}{1}{section.1.2}
\contentsline {section}{\numberline {1.3}Utilitate}{1}{section.1.3}
\contentsline {section}{\numberline {1.4}Structura lucrării}{2}{section.1.4}
\contentsline {chapter}{\numberline {2}Instrumente de dezvoltare}{3}{chapter.2}
\contentsline {section}{\numberline {2.1}Visual Studio}{3}{section.2.1}
\contentsline {subsection}{\numberline {2.1.1}Arhitectură}{4}{subsection.2.1.1}
\contentsline {subsection}{\numberline {2.1.2}Caracteristici}{4}{subsection.2.1.2}
\contentsline {subsection}{\numberline {2.1.3}Extensibilitate}{7}{subsection.2.1.3}
\contentsline {section}{\numberline {2.2}Biblioteci dinamice}{7}{section.2.2}
\contentsline {section}{\numberline {2.3}Android Studio}{8}{section.2.3}
\contentsline {subsection}{\numberline {2.3.1}Arhitectură}{8}{subsection.2.3.1}
\contentsline {subsection}{\numberline {2.3.2}Caracteristici}{11}{subsection.2.3.2}
\contentsline {subsection}{\numberline {2.3.3}Avantaje}{12}{subsection.2.3.3}
\contentsline {section}{\numberline {2.4}CMake și Ez-gen}{12}{section.2.4}
\contentsline {section}{\numberline {2.5}Source Control}{14}{section.2.5}
\contentsline {subsection}{\numberline {2.5.1}Bitbucket}{14}{subsection.2.5.1}
\contentsline {subsection}{\numberline {2.5.2}SourceTree}{15}{subsection.2.5.2}
\contentsline {chapter}{\numberline {3}Arhitectura aplicației}{17}{chapter.3}
\contentsline {section}{\numberline {3.1}Componente}{17}{section.3.1}
\contentsline {subsection}{\numberline {3.1.1}Server}{18}{subsection.3.1.1}
\contentsline {subsection}{\numberline {3.1.2}Resurse dll - Jocuri}{23}{subsection.3.1.2}
\contentsline {subsubsection}{PaperRockScissors}{26}{section*.12}
\contentsline {subsubsection}{FiveInLine}{29}{section*.14}
\contentsline {subsubsection}{Chat}{32}{section*.16}
\contentsline {subsection}{\numberline {3.1.3}Resurse dll - Loaders}{33}{subsection.3.1.3}
\contentsline {subsubsection}{DllLoader}{34}{section*.19}
\contentsline {subsubsection}{TcpIpLoader}{37}{section*.21}
\contentsline {subsection}{\numberline {3.1.4}Jucator - Fisier dll}{39}{subsection.3.1.4}
\contentsline {subsection}{\numberline {3.1.5}Jucator - TCP/IP}{44}{subsection.3.1.5}
\contentsline {section}{\numberline {3.2}Cazuri de utilizare}{49}{section.3.2}
\contentsline {chapter}{\numberline {4}Scenariu de utilizare}{51}{chapter.4}
\contentsline {section}{\numberline {4.1}Scenariu folosind fișiere dll}{51}{section.4.1}
\contentsline {section}{\numberline {4.2}Scenariu folosind conexiune TCP/IP}{53}{section.4.2}
\contentsline {chapter}{\numberline {5}Concluzii}{58}{chapter.5}

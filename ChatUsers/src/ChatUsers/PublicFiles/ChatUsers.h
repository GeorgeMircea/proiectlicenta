#pragma once
#define WIN32_LEAN_AND_MEAN

#include <ws2tcpip.h>
#include<sstream>
#include<array>

#include<iostream>

#include "Headers.h"

#pragma comment (lib, "Ws2_32.lib")

#define DEFAULT_BUFLEN 512
#define DEFAULT_PORT "27015"

class ChatUsers :
	public IPlayer
{
private:
	char* recvbuf;
	int iResult;
	int recvbuflen = DEFAULT_BUFLEN;

	std::string m_name;
	std::string m_message;

	//in case of server crush
	bool m_error;

	bool hasToAnswer=true;

	WSADATA wsaData;
	SOCKET ConnectSocket = INVALID_SOCKET;
	struct addrinfo *result = NULL,
		*ptr = NULL,
		hints;

	int initialize(char ** argv);

	void displayMessage();

public:
	ChatUsers(char ** argv);
	virtual ~ChatUsers();

	// Inherited via IPlayer
	virtual std::string sendMessage() override final;
	virtual void receiveMessage(const std::string& message) override final;
	virtual void setName(const std::string& name) override final;
	virtual std::string getName() override final;


	void play();
};


#include "ChatUsers.h"

//#define fiveInLine 0
//#define paper !fiveInLine

int ChatUsers::initialize(char ** argv)
{
	// Initialize Winsock
	iResult = WSAStartup(MAKEWORD(2, 2), &wsaData);
	if (iResult != 0)
	{
		printf("WSAStartup failed with error: %d\n", iResult);
		return 1;
	}

	ZeroMemory(&hints, sizeof(hints));
	hints.ai_family = AF_UNSPEC;
	hints.ai_socktype = SOCK_STREAM;
	hints.ai_protocol = IPPROTO_TCP;

	// Resolve the server address and port
	iResult = getaddrinfo(argv[1], DEFAULT_PORT, &hints, &result);
	if (iResult != 0)
	{
		printf("getaddrinfo failed with error: %d\n", iResult);
		WSACleanup();
		return 1;
	}

	// Attempt to connect to an address until one succeeds
	for (ptr = result; ptr != NULL;ptr = ptr->ai_next)
	{

		// Create a SOCKET for connecting to server
		ConnectSocket = socket(ptr->ai_family, ptr->ai_socktype,
			ptr->ai_protocol);
		if (ConnectSocket == INVALID_SOCKET)
		{
			printf("socket failed with error: %ld\n", WSAGetLastError());
			WSACleanup();
			return 1;
		}

		// Connect to server.
		iResult = connect(ConnectSocket, ptr->ai_addr, (int)ptr->ai_addrlen);
		if (iResult == SOCKET_ERROR)
		{
			closesocket(ConnectSocket);
			ConnectSocket = INVALID_SOCKET;
			continue;
		}
		break;
	}

	freeaddrinfo(result);

	if (ConnectSocket == INVALID_SOCKET)
	{
		printf("Unable to connect to server!\n");
		WSACleanup();
		return 1;
	}

	m_error = false;

	return 0;
}

void ChatUsers::displayMessage()
{
	if('@' == m_message[0])
		std::cout << m_message.substr(1) << std::endl;
	if ("Play again? (y/n)" == m_message)
	{
		std::cout << "Connection over" << std::endl;
		m_message = "/close";
	}

}


ChatUsers::ChatUsers(char ** argv)
{
	initialize(argv);
}

ChatUsers::~ChatUsers()
{
	closesocket(ConnectSocket);
	WSACleanup();
}

std::string ChatUsers::sendMessage()
{
	std::string message;
	if ("/close" != m_message)
	{
		std::getline(std::cin, message);
	}
	else
	{
		m_message = "n";
	}

	char *sendbuf = new char[message.length() + 1];
	strcpy(sendbuf, message.c_str());

	iResult = send(ConnectSocket, sendbuf, (int)strlen(sendbuf), 0);
	if (iResult == SOCKET_ERROR)
	{
		printf("send failed with error: %d\n", WSAGetLastError());
		closesocket(ConnectSocket);
		WSACleanup();
		return "";
	}
	delete[] sendbuf;
	//printf("Bytes Sent: %ld\n", iResult);
	return message;
}

void ChatUsers::receiveMessage(const std::string& message)
{
	m_message = "";

	recvbuf = new char[DEFAULT_BUFLEN];
	memset(&recvbuf[0], '\0', DEFAULT_BUFLEN);
	iResult = recv(ConnectSocket, recvbuf, recvbuflen, 0);
	if (iResult > 0)
	{
		m_message.assign(recvbuf, iResult);
		if ("Your turn" == m_message || "Invalid message. Please try again!" == m_message || "Play again? (y/n)" == m_message)
			hasToAnswer = true;
		else
			hasToAnswer = false;
		displayMessage();
	}
	else if (iResult == 0)
		std::cout << "Connection closed\n";
	else
	{
		std::cout << "Server crushed!\n";
		m_error = true;
	}

	delete[] recvbuf;
}

void ChatUsers::setName(const std::string& name)
{
	m_name = name;
}

std::string ChatUsers::getName()
{
	char *sendbuf = new char[m_name.length() + 1];
	strcpy(sendbuf, m_name.c_str());

	iResult = send(ConnectSocket, sendbuf, (int)strlen(sendbuf), 0);
	if (iResult == SOCKET_ERROR)
	{
		printf("send failed with error: %d\n", WSAGetLastError());
		closesocket(ConnectSocket);
		WSACleanup();
		delete[] sendbuf;
		return "";
	}
	delete[] sendbuf;
	printf("Bytes Sent: %ld\n", iResult);

	return m_name;
}

void ChatUsers::play()
{
	std::cout << "Your name: " << std::endl;

	std::string name;
	std::getline(std::cin, name);

	setName(name);
	if (getName() == "")
	{
		return;
	}

	do
	{
		receiveMessage("");
		if (hasToAnswer && !m_error)
			sendMessage();
	} while (m_message != "Game Over!" && m_message!="n" && !m_error);
}

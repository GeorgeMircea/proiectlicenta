#include "PaperRockScissors.h"


IGame & PaperRockScissors::getInstance()
{
	static PaperRockScissors instance;
	return instance;
}

void PaperRockScissors::decideWinner()
{
	if (firstPlayerWins())
	{
		m_winner = Winner::PLAYER_ONE;
		return;
	}
	if (secondPlayerWins())
	{
		m_winner = Winner::PLAYER_TWO;
		return;
	}
	if (isDraw())
	{
		m_winner = Winner::DRAW;
		return;
	}

	m_winner = Winner::UNDISPUTED;
}

std::string PaperRockScissors::getGameName()
{
	return "PaperRockScissors";
}

bool PaperRockScissors::gameEnded()
{
	if (m_firstPlayerMove != Move::UNKNOWN && m_secondPlayerMove != Move::UNKNOWN)
	{
		decideWinner();
		return true;
	}
	return false;
}

bool PaperRockScissors::hasBoard()
{
	return false;
}

void PaperRockScissors::restart()
{

	//all moves are reseted
	m_firstPlayerMove = Move::UNKNOWN;
	m_secondPlayerMove = Move::UNKNOWN;
	m_winner = Winner::UNDISPUTED;
}

std::string PaperRockScissors::getMessage()
{
	if (m_secondPlayerMove == Move::UNKNOWN)
	{
		return "";
	}
	std::string message = moveConverter(m_firstPlayerMove) + " " + moveConverter(m_secondPlayerMove);
	return message;
}
bool PaperRockScissors::setMessage(const std::string& message)
{
	std::string move="";
	std::string player="";
	
	if (!messageIsValid(message, move, player))
		return false;

	interpretMessage(move, player);
	return true;
}
void PaperRockScissors::interpretMessage(const std::string& move, const std::string& player)
{
	if (player == "Player1")
	{
		m_firstPlayerMove = moveConverter(move);
		return;
	}
	if (player == "Player2")
	{
		m_secondPlayerMove = moveConverter(move);
		return;
	}
}

bool PaperRockScissors::messageIsValid(const std::string& message, std::string& move, std::string& player)
{
	std::stringstream ss(message);

	ss >> move;
	ss >> player;
	
	if (move!="Rock" && move!="Paper" && move!="Scissors")
		return false;
	if (player != "Player1" && player != "Player2")
		return false;
	
	return true;
}

std::string PaperRockScissors::moveConverter(Move move)
{
	switch (move)
	{
	case Move::PAPER:
		return "Paper";
	case Move::ROCK:
		return "Rock";
	case Move::SCISSORS:
		return "Scissors";
	default:
		return "Unkonwn";
	}
}
PaperRockScissors::Move PaperRockScissors::moveConverter(const std::string& move)
{
	if(move=="Paper")
		return Move::PAPER;
	if (move == "Rock")
		return Move::ROCK;
	if (move == "Scissors")
		return Move::SCISSORS;
	return Move::UNKNOWN;
}

Winner PaperRockScissors::getWinner() const
{
	return m_winner;
}

bool PaperRockScissors::firstPlayerWins() const
{
	if (m_firstPlayerMove == Move::PAPER && m_secondPlayerMove == Move::ROCK)
		return true;
	if (m_firstPlayerMove == Move::ROCK && m_secondPlayerMove == Move::SCISSORS)
		return true;
	if (m_firstPlayerMove == Move::SCISSORS && m_secondPlayerMove == Move::PAPER)
		return true;

	return false;
}
bool PaperRockScissors::secondPlayerWins() const
{
	if (m_secondPlayerMove == Move::PAPER && m_firstPlayerMove == Move::ROCK)
		return true;
	if (m_secondPlayerMove == Move::ROCK && m_firstPlayerMove == Move::SCISSORS)
		return true;
	if (m_secondPlayerMove == Move::SCISSORS && m_firstPlayerMove == Move::PAPER)
		return true;

	return false;
}
bool PaperRockScissors::isDraw() const
{
	return m_firstPlayerMove == m_secondPlayerMove;
}

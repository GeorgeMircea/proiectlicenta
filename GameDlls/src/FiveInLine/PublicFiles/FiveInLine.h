#pragma once
#include"Headers.h"
#include<array>

class FiveInLine :
	public IGame
{
	static const int BOARD_WIDTH = 10;
	static const int BOARD_HEIGHT = 10;

	std::array<std::array<std::string, BOARD_WIDTH>, BOARD_HEIGHT> m_board;
	int m_numberOfMoves = 0;

	struct CurrentMove
	{
		int line=-1;
		int column=-1;
		std::string symbol;
	};

	CurrentMove m_currentMove;

	Winner m_winner = Winner::UNDISPUTED;

	bool firstPlayerWins() const;
	bool secondPlayerWins() const;
	bool isDraw() const;

	bool check5inLine(int start, int end) const;
	bool checkLine() const;

	bool check5inColumn(int start, int end) const;
	bool checkColumn() const;

	bool checkFirstDiagonal(int line, int column) const;
	bool checkSecondDiagonal(int line, int column) const;
	bool checkDiagonals() const;

	//intreprets the message
	void interpretMessage(int line, int column, const std::string& player);

	//checks if a received message is valid
	//move and player are read from the message
	bool messageIsValid(const std::string& message, int& line, int& column, std::string& player);

	//initialize the board with empty string ("")
	void initializeBoard();

	//only calls the initializeBoard() method
	FiveInLine();
	
public:
	virtual ~FiveInLine() = default;

	static IGame& getInstance();
	// Inherited via IGame
	virtual void decideWinner() override;
	virtual Winner getWinner() const override;
	virtual std::string getGameName() override;
	virtual bool gameEnded() override;
	virtual bool hasBoard() override;
	virtual void restart() override;

	//the message contains the line, the column and the player who played
	//delimited by space
	virtual bool setMessage(const std::string& message) override;

	//the message contains the line, the column and the symbol played
	virtual std::string getMessage() override;
};

extern "C"
{
	__declspec(dllexport) IGame* getGame();
};


//create a FiveInLine instance
IGame* getGame()
{
	return &FiveInLine::getInstance();
}

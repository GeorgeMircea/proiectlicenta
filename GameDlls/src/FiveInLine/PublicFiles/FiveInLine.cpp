#include "FiveInLine.h"


IGame & FiveInLine::getInstance()
{
	static FiveInLine instance;
	return instance;
}

void FiveInLine::decideWinner()
{
	if (firstPlayerWins())
	{
		m_winner = Winner::PLAYER_ONE;
		return;
	}
	if (secondPlayerWins())
	{
		m_winner = Winner::PLAYER_TWO;
		return;
	}
	if (isDraw())
	{
		m_winner = Winner::DRAW;
		return;
	}

	m_winner = Winner::UNDISPUTED;
}

std::string FiveInLine::getGameName()
{
	return "FiveInLine";
}

bool FiveInLine::gameEnded()
{
	if (m_currentMove.line == -1 || m_currentMove.column == -1)
	{
		m_winner = Winner::UNDISPUTED;
		return false;
	}
	decideWinner();
	
	//if there is a winner, the game ended
	if (m_winner != Winner::UNDISPUTED)
		return true;

	return false;
}

bool FiveInLine::hasBoard()
{
	return true;
}

void FiveInLine::restart()
{
	//reset all game
	initializeBoard();
	m_currentMove.line = -1;
	m_currentMove.column = -1;
	m_winner = Winner::UNDISPUTED;

}

std::string FiveInLine::getMessage()
{
	std::string message = std::to_string(m_currentMove.line) + " " + std::to_string(m_currentMove.column) + " " + m_currentMove.symbol;
	return message;
}
bool FiveInLine::setMessage(const std::string& message)
{
	int line;
	int column;
	std::string player;

	if (!messageIsValid(message, line, column, player))
		return false;

	interpretMessage(line, column, player);
	return true;
}
void FiveInLine::interpretMessage(int line, int column, const std::string& player)
{
	m_currentMove.line = line;
	m_currentMove.column = column;
	if (player == "Player1")
	{
		m_currentMove.symbol = "X";
	}
	if (player == "Player2")
	{
		m_currentMove.symbol = "0";
	}
	m_board[m_currentMove.line][m_currentMove.column] = m_currentMove.symbol;
}

bool FiveInLine::messageIsValid(const std::string & message, int& line, int& column, std::string & player)
{
	std::stringstream ss(message);

	ss >>line;
	if (line < 0 || line >= BOARD_HEIGHT)
		return false;

	ss >> column;
	if (column < 0 || column >= BOARD_WIDTH)
		return false;

	ss >> player;
	if(player!="Player1" && player!="Player2")
		return false;

	if ("" != m_board[line][column])
		return false;

	return true;
}

void FiveInLine::initializeBoard()
{
	for (int i = 0; i < BOARD_HEIGHT; ++i)
		for (int j = 0; j < BOARD_WIDTH; ++j)
			m_board[i][j] = "";
}

FiveInLine::FiveInLine()
{
	initializeBoard();
}

Winner FiveInLine::getWinner() const
{
	return m_winner;
}

bool FiveInLine::firstPlayerWins() const
{
	if (m_currentMove.symbol == "0")
		return false;

	return checkLine() || checkColumn() || checkDiagonals();
}
bool FiveInLine::secondPlayerWins() const
{
	if (m_currentMove.symbol == "X")
		return false;

	return checkLine() || checkColumn() || checkDiagonals();
}
bool FiveInLine::isDraw() const
{
	if (m_numberOfMoves == BOARD_WIDTH*BOARD_HEIGHT)
		return true;
	return false;
}

bool FiveInLine::check5inLine(int start, int end) const
{
	int longestSequence = 0;

	for (int column = start; column < end; ++column)
	{
		if (m_board[m_currentMove.line][column] == m_currentMove.symbol)
		{
			if (++longestSequence == 5)
				return true;
		}
		else
		{
			longestSequence = 0;
		}
	}
	return false;
}
bool FiveInLine::checkLine() const
{
	if (m_currentMove.column < 5)
	{
		if (check5inLine(0, m_currentMove.column + 5))
			return true;
	}
	else if (BOARD_WIDTH - m_currentMove.column < 5)
	{
		if (check5inLine(m_currentMove.column - 4, BOARD_WIDTH))
			return true;
	}
	else if (check5inLine(m_currentMove.column - 4, m_currentMove.column + 5))
		return true;

	return false;
}

bool FiveInLine::check5inColumn(int start, int end) const
{
	int longestSequence = 0;

	for (int line = start; line < end; ++line)
	{
		if (m_board[line][m_currentMove.column] == m_currentMove.symbol)
		{
			if (++longestSequence == 5)
				return true;
		}
		else
		{
			longestSequence = 0;
		}
	}
	return false;
}
bool FiveInLine::checkColumn() const
{
	if (m_currentMove.line < 5)
	{
		if (check5inColumn(0, m_currentMove.line + 5))
			return true;
	}
	else if (BOARD_WIDTH - m_currentMove.line < 5)
	{
		if (check5inColumn(m_currentMove.line - 4, BOARD_WIDTH))
			return true;
	}
	else if (check5inColumn(m_currentMove.line - 4, m_currentMove.line + 5))
		return true;

	return false;
}

bool FiveInLine::checkFirstDiagonal(int line, int column) const
{
	auto i = line;
	auto j = column;

	if (line != 0 && column != 0)
	{
		//go to the element from the left or the top of the board
		do
		{
			i--;
			j--;
		} while (i != 0 && j != 0 && i >= line - 4);
	}


	//check all the elements from the diagonal containing the current move
	int longestSequence = 0;
	for (; i < BOARD_HEIGHT && j < BOARD_WIDTH && i < line + 5; ++i, ++j)
	{
		if (m_board[i][j] == m_currentMove.symbol)
		{
			if (++longestSequence == 5)
				return true;
		}
		else
		{
			longestSequence = 0;
		}
	}
	return false;
}
bool FiveInLine::checkSecondDiagonal(int line, int column) const
{
	auto i = line;
	auto j = column;
	
	if (line != 0 && column != BOARD_WIDTH-1)
	{
		//go to the element from the right or the top of the board
		do
		{
			i--;
			j++;
		} while (i != 0 && j != BOARD_WIDTH-1 && i >= line - 4);
	}

	//check all the elements from the diagonal containing the current move
	int longestSequence = 0;
	for (; i < BOARD_HEIGHT && j >=0 && i < line + 5; ++i, --j)
	{
		if (m_board[i][j] == m_currentMove.symbol)
		{
			if (++longestSequence == 5)
				return true;
		}
		else
		{
			longestSequence = 0;
		}
	}

	return false;
}

bool FiveInLine::checkDiagonals() const
{
	return checkFirstDiagonal(m_currentMove.line, m_currentMove.column) ||checkSecondDiagonal(m_currentMove.line, m_currentMove.column);
}

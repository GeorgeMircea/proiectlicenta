#pragma once
#include"Headers.h"

class Chat :
	public IGame
{	
public:
	virtual ~Chat() = default;

	static IGame& getInstance();
	// Inherited via IGame
	virtual void decideWinner() override;
	virtual Winner getWinner() const override;
	virtual std::string getGameName() override;
	virtual bool gameEnded() override;
	virtual bool hasBoard() override;
	virtual void restart() override;

	virtual bool setMessage(const std::string& message) override;
	virtual std::string getMessage() override;


	void interpretMessage(std::string message);
private:
	Winner m_closer=Winner::UNDISPUTED;
	
	std::string m_conversation="";
	std::string m_message="";

	Chat() = default;
};

extern "C"
{
	__declspec(dllexport) IGame* getGame();
};


//create a Chat instance
IGame* getGame()
{
	return &Chat::getInstance();
}

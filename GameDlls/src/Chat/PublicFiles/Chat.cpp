#include"Chat.h"

IGame & Chat::getInstance()
{
	static Chat instance;
	return instance;
}

void Chat::decideWinner()
{
	std::stringstream ss(m_message);
	std::string word, player, lastWord = "";

	ss >> player;
	ss >> word;
	while (word != lastWord)
	{
		lastWord = word;
		if ("/close" == word)
		{
			if ("Player1:" == player)
			{
				m_closer = Winner::PLAYER_ONE;
				return;
			}
			if ("Player2:" == player)
			{
				m_closer = Winner::PLAYER_TWO;
				return;
			}
		}
		ss >> word;
	}
}

Winner Chat::getWinner() const
{
	return m_closer;
}

std::string Chat::getGameName()
{
	return "Chat";
}

bool Chat::gameEnded()
{
	decideWinner();

	if (Winner::UNDISPUTED != m_closer)
		return true;
	return false;
}

bool Chat::hasBoard()
{
	return false;
}

void Chat::restart()
{

}

bool Chat::setMessage(const std::string & message)
{
	interpretMessage(message);
	return true;
}

std::string Chat::getMessage()
{
	return m_message;
}

void Chat::interpretMessage(std::string message)
{
	std::stringstream ss(message);

	std::string word = "";
	m_message = "";
	do
	{
		m_message = m_message + " " + word;
		ss >> word;
	} while ("Player1" != word && "Player2" != word);

	m_conversation = m_conversation + word + ": ";
	m_conversation += message;
	m_conversation.push_back('\n');

	m_message = word + ": " + m_message;
}


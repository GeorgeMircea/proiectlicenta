#pragma once
#define WIN32_LEAN_AND_MEAN

#include <ws2tcpip.h>
#include<sstream>
#include<array>

#include<iostream>

#include "Headers.h"
#include"PaperRockScissorsBoard.h"
#include"FiveInLineBoard.h"

#pragma comment (lib, "Ws2_32.lib")

#define DEFAULT_BUFLEN 512
#define DEFAULT_PORT "27015"

class TcpIpPaperRockScissors :
	public IPlayer
{
private:
	char* recvbuf;
	int iResult;
	int recvbuflen = DEFAULT_BUFLEN;


	std::string m_name;
	std::string m_message;
	std::string m_game;
	IBoard* m_board;
	bool hasToAnswer=true;

	//in case of server crush
	bool m_error;

	WSADATA wsaData;
	SOCKET ConnectSocket = INVALID_SOCKET;
	struct addrinfo *result = NULL,
		*ptr = NULL,
		hints;

	int initialize(char ** argv);


	//std::array<std::array<std::string, 10>, 10> m_board;

	void showBoard();
	void chooseGame();


public:
	TcpIpPaperRockScissors(char ** argv);
	virtual ~TcpIpPaperRockScissors();

	// Inherited via IPlayer
	virtual std::string sendMessage() override final;
	virtual void receiveMessage(const std::string& message) override final;
	virtual void setName(const std::string& name) override final;
	virtual std::string getName() override final;


	void play();
};


#pragma once
#include "IBoard.h"

#include<iostream>
#include<array>
#include<sstream>


class FiveInLineBoard :
	public IBoard
{
public:
	FiveInLineBoard();
	virtual ~FiveInLineBoard() = default;

	// Inherited via IBoard
	virtual void showBoard(std::string move) override;
	virtual void initializeBoard() override;

private:
	static const int BOARD_HEIGHT = 10;
	static const int BOARD_WIDTH = 10;

	std::array<std::array<std::string, BOARD_WIDTH>, BOARD_HEIGHT > m_board;

	void interpretMove(std::string move);
};


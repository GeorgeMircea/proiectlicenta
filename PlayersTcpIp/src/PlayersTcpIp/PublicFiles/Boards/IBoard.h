#pragma once
#include<string>

class IBoard
{
public:

	IBoard()=default;
	virtual ~IBoard() = default;

	//interprets the move and displays the board
	virtual void showBoard(std::string move) = 0;

	//initializes the board
	virtual void initializeBoard() = 0;
};
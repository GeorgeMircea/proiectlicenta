#include "FiveInLineBoard.h"



FiveInLineBoard::FiveInLineBoard()
{
	initializeBoard();
}

void FiveInLineBoard::showBoard(std::string move)
{
	interpretMove(move);

	for (auto i = 0; i < BOARD_HEIGHT; i++)
	{
		for (auto j = 0; j < BOARD_WIDTH; j++)
			std::cout << m_board[i][j];
		std::cout << std::endl;
	}
}

void FiveInLineBoard::initializeBoard()
{
	for (auto i = 0; i < BOARD_HEIGHT; i++)
		for (auto j = 0; j < BOARD_WIDTH; j++)
			m_board[i][j] = "_";
}

void FiveInLineBoard::interpretMove(std::string move)
{
	std::stringstream ss(move);
	int line;
	int column;
	char symbol;

	ss >> symbol;
	ss >> line;
	ss >> column;
	ss >> symbol;
	ss >> symbol;

	m_board[line][column] = symbol;
}

#pragma once
#include"IBoard.h"

class PaperRockScissorsBoard: public IBoard
{
public:
	PaperRockScissorsBoard();
	virtual ~PaperRockScissorsBoard();

	// Inherited via IBoard
	virtual void showBoard(std::string move) override;
	virtual void initializeBoard() override;
};


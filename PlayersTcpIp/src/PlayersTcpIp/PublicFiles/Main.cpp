#define WIN32_LEAN_AND_MEAN

#include"TcpIpPaperRockScissors.h"

#define DEFAULT_BUFLEN 512
#define DEFAULT_PORT "27015"

char* recvbuf;
int iResult;
int recvbuflen = DEFAULT_BUFLEN;

int main(int argc, char **argv) 
{

	TcpIpPaperRockScissors player(argv);
	player.play();

    return 0;
}

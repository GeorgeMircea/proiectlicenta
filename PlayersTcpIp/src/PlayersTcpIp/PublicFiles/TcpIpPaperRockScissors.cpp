#include "TcpIpPaperRockScissors.h"

//#define fiveInLine 0
//#define paper !fiveInLine

int TcpIpPaperRockScissors::initialize(char ** argv)
{
	// Initialize Winsock
	iResult = WSAStartup(MAKEWORD(2, 2), &wsaData);
	if (iResult != 0)
	{
		printf("WSAStartup failed with error: %d\n", iResult);
		return 1;
	}

	ZeroMemory(&hints, sizeof(hints));
	hints.ai_family = AF_UNSPEC;
	hints.ai_socktype = SOCK_STREAM;
	hints.ai_protocol = IPPROTO_TCP;

	// Resolve the server address and port
	iResult = getaddrinfo(argv[1], DEFAULT_PORT, &hints, &result);
	if (iResult != 0)
	{
		printf("getaddrinfo failed with error: %d\n", iResult);
		WSACleanup();
		return 1;
	}

	// Attempt to connect to an address until one succeeds
	for (ptr = result; ptr != NULL;ptr = ptr->ai_next)
	{

		// Create a SOCKET for connecting to server
		ConnectSocket = socket(ptr->ai_family, ptr->ai_socktype,
			ptr->ai_protocol);
		if (ConnectSocket == INVALID_SOCKET)
		{
			printf("socket failed with error: %ld\n", WSAGetLastError());
			WSACleanup();
			return 1;
		}

		// Connect to server.
		iResult = connect(ConnectSocket, ptr->ai_addr, (int)ptr->ai_addrlen);
		if (iResult == SOCKET_ERROR)
		{
			closesocket(ConnectSocket);
			ConnectSocket = INVALID_SOCKET;
			continue;
		}
		break;
	}

	freeaddrinfo(result);

	if (ConnectSocket == INVALID_SOCKET)
	{
		printf("Unable to connect to server!\n");
		WSACleanup();
		return 1;
	}

	m_error = false;

	return 0;
}

void TcpIpPaperRockScissors::showBoard()
{
	m_board->showBoard(m_message);
}

void TcpIpPaperRockScissors::chooseGame()
{
	std::cout << "1 - Paper-Rock-Scissors\n2 - Five in Line\n";

	int option;
	do
	{
		std::cin >> option;
	} while (option < 1 || option >2);

	switch (option)
	{
	case 1:
		m_game = "PaperRockScissors";
		m_board = new PaperRockScissorsBoard();
		break;
	case 2:
		m_game = "FiveInLine";
		m_board = new FiveInLineBoard();
		break;
	default:
		break;
	}
	std::cin.get();
}

TcpIpPaperRockScissors::TcpIpPaperRockScissors(char ** argv)
{
	initialize(argv);
}

TcpIpPaperRockScissors::~TcpIpPaperRockScissors()
{
	closesocket(ConnectSocket);
	WSACleanup();
}

std::string TcpIpPaperRockScissors::sendMessage()
{
	std::string message;
	std::getline(std::cin, message);

	char *sendbuf = new char[message.length() + 1];
	strcpy(sendbuf, message.c_str());

	iResult = send(ConnectSocket, sendbuf, (int)strlen(sendbuf), 0);
	if (iResult == SOCKET_ERROR)
	{
		printf("send failed with error: %d\n", WSAGetLastError());
		closesocket(ConnectSocket);
		WSACleanup();
		m_error = true;
		return "";
	}
	delete[] sendbuf;
	printf("Bytes Sent: %ld\n", iResult);
	return message;
}

void TcpIpPaperRockScissors::receiveMessage(const std::string& message)
{
	m_message = "";

	recvbuf = new char[DEFAULT_BUFLEN];
	memset(&recvbuf[0], '\0', DEFAULT_BUFLEN);
	iResult = recv(ConnectSocket, recvbuf, recvbuflen, 0);
	if (iResult > 0)
	{
		m_message.assign(recvbuf, iResult);
		std::cout << m_message << std::endl;
		if ("Your turn" == m_message || "Invalid message. Please try again!" == m_message || "Play again? (y/n)" == m_message)
			hasToAnswer = true;
		else
			hasToAnswer = false;
		
		if ("Play again? (y/n)" == m_message)
			m_board->initializeBoard();

//#if fiveInLine
		if ('@' == m_message[0])
			showBoard();
//#endif
	}
	else if (iResult == 0)
		std::cout << "Connection closed\n";
	else
	{
		std::cout << "Server crushed!\n";
		m_error = true;
	}
	delete[] recvbuf;
}

void TcpIpPaperRockScissors::setName(const std::string& name)
{
	m_name = name;
}

std::string TcpIpPaperRockScissors::getName()
{
	char *sendbuf = new char[m_name.length() + 1];
	strcpy(sendbuf, m_name.c_str());

	iResult = send(ConnectSocket, sendbuf, (int)strlen(sendbuf), 0);
	if (iResult == SOCKET_ERROR)
	{
		printf("send failed with error: %d\n", WSAGetLastError());
		closesocket(ConnectSocket);
		WSACleanup();
		delete[] sendbuf;
		return "";
	}
	delete[] sendbuf;
	printf("Bytes Sent: %ld\n", iResult);

	return m_name;
}

void TcpIpPaperRockScissors::play()
{
	std::cout << "Your name: " << std::endl;

	std::string name;
	std::getline(std::cin, name);

	setName(name);
	if (getName() == "")
	{
		return;
	}

	chooseGame();

	do
	{
		receiveMessage("");
		if (hasToAnswer && !m_error)
			sendMessage();
	} while (m_message != "Game Over!" && !m_error);
}

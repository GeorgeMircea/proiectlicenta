#pragma once
#include"Headers.h"
#include<random>

class Player :
	public IPlayer
{
private:
	std::string m_name;

protected:
	std::string m_message;

public:
	Player();
	virtual ~Player() = default;

	// Inherited via IPlayer
	void receiveMessage(const std::string& message) final;
	void setName(const std::string& name) final;
	std::string getName() final;
	std::string sendMessage() final;

	virtual void interpretMessage(const std::string& message) = 0;
	virtual void strategy() = 0;
};
#pragma once
#include<Player.h>
#include<random>

class Player1 :
	public Player
{
private:
	Player1() = default;
public:

	static IPlayer& getInstance();
	Player1(std::string name);
	virtual ~Player1() = default;


	// Inherited via Player
	virtual void strategy() override;
	virtual void interpretMessage(const std::string& message) override;

};

extern "C"
{
	__declspec(dllexport) IPlayer* getPlayer();
};


//create a PaperRockScissors instance
IPlayer* getPlayer()
{
	return &Player1::getInstance();
}

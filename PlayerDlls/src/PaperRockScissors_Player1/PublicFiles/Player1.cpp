#include "Player1.h"



IPlayer & Player1::getInstance()
{
	static Player1 instance;
	return instance;
}

Player1::Player1(std::string name)
{
	setName(name);
}

void Player1::strategy()
{
	const auto NUMBER_OF_POSSIBLE_CHOICES = 3;
	std::random_device rd;
	std::mt19937 gen(rd());
	std::uniform_int_distribution<> dis(0, NUMBER_OF_POSSIBLE_CHOICES - 1);

	int choice = dis(gen);
	switch (choice)
	{
	case 0:
		m_message = "Paper";
		return;
	case 1:
		m_message = "Rock";
		return;
	case 2:
		m_message = "Scissors";
		return;
	default:
		m_message = "";
		return;
	}
}

void Player1::interpretMessage(const std::string& message)
{
	return;
}

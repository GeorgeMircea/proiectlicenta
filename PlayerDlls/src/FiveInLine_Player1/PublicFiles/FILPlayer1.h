#pragma once
#include<Player.h>

#include<array>
#include<sstream>
#include<iostream>

class FILPlayer1 :
	public Player
{
	static const int BOARD_WIDTH = 10;
	static const int BOARD_HEIGHT = 10;
	std::array<std::array<std::string, BOARD_WIDTH>, BOARD_HEIGHT> m_board;

	//used for basic tests
	int m_it=0;

	void showBoard();

	FILPlayer1();
	
public:
	static IPlayer& getInstance();
	FILPlayer1(std::string name);
	virtual ~FILPlayer1() = default;


	// Inherited via Player
	virtual void strategy() override;
	virtual void interpretMessage(const std::string& message) override;

};

extern "C"
{
	__declspec(dllexport) IPlayer* getPlayer();
};


//create a PaperRockScissors instance
IPlayer* getPlayer()
{
	return &FILPlayer1::getInstance();
}

#include "FILPlayer1.h"


void FILPlayer1::showBoard()
{
	for (int i = 0; i < BOARD_HEIGHT; i++)
	{
		for (int j = 0; j < BOARD_WIDTH; ++j)
			std::cout << m_board[i][j];
		std::cout << std::endl;
	}
	std::cout << std::endl;
}

FILPlayer1::FILPlayer1()
{
	for (int i = 0; i < BOARD_HEIGHT; ++i)
		for (int j = 0; j < BOARD_WIDTH; ++j)
			m_board[i][j] = "_";
}

IPlayer & FILPlayer1::getInstance()
{
	static FILPlayer1 instance;
	return instance;
}

FILPlayer1::FILPlayer1(std::string name)
{
	setName(name);
}

void FILPlayer1::strategy()
{
	//showBoard();

	std::random_device rd;
	std::mt19937 gen(rd());
	std::uniform_int_distribution<> dis(0, 9);

	int line = dis(gen);
	int column = dis(gen);
	m_message = std::to_string(line) + " " + std::to_string(column);

	m_board[line][column] = "A";

	//showBoard();

}

void FILPlayer1::interpretMessage(const std::string& message)
{
	if (message == "Start" || message=="You win!" || message=="You lose!" || message=="It's a draw!" || message=="y")
		return;

	std::stringstream ss(message);
	int line;
	int column;
	std::string symbol;

	ss >> line;
	ss >> column;
	ss >> symbol;

	if("_"==m_board[line][column])
		m_board[line][column] = symbol;
}

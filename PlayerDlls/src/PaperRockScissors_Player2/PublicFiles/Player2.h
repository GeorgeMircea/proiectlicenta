#pragma once
#include<Player.h>
class Player2 :
	public Player
{
private:
	int index = 0;
	Player2() = default;

public:
	static IPlayer& getInstance();
	Player2(std::string name);
	virtual ~Player2() = default;



	// Inherited via Player
	virtual void strategy() override;
	virtual void interpretMessage(const std::string& message) override;

};

extern "C"
{
	__declspec(dllexport) IPlayer* getPlayer();
};


//create a PaperRockScissors instance
IPlayer* getPlayer()
{
	return &Player2::getInstance();
}
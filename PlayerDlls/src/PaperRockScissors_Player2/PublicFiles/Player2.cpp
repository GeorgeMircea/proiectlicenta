#include "Player2.h"


IPlayer & Player2::getInstance()
{
	static Player2 instance;
	return instance;
}

Player2::Player2(std::string name)
{
	setName(name);
}

void Player2::strategy()
{
	++index;
	index %= 3;
	switch (index)
	{
	case 0:
		m_message="Paper";
		return;
	case 1:
		m_message="Rock";
		return;
	case 2:
		m_message="Scissors";
		return;
	default:
		m_message="";
		return;
	}
}

void Player2::interpretMessage(const std::string& message)
{
	return;
}

#include "FILPlayer2.h"


IPlayer & FILPlayer2::getInstance()
{
	static FILPlayer2 instance;
	return instance;
}

FILPlayer2::FILPlayer2(std::string name)
{
	setName(name);
}

void FILPlayer2::strategy()
{
	std::random_device rd;
	std::mt19937 gen(rd());
	std::uniform_int_distribution<> dis(0, 9);

	int line = dis(gen);
	int column = dis(gen);
	m_message = std::to_string(line) + " " + std::to_string(column);
}

void FILPlayer2::interpretMessage(const std::string& message)
{
	return;
}


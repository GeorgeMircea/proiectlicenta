#pragma once
#include<Player.h>
#include<random>

class FILPlayer2 :
	public Player
{
private:
	int m_it = 0;
	FILPlayer2() = default;

public:
	static IPlayer& getInstance();
	FILPlayer2(std::string name);
	virtual ~FILPlayer2() = default;


	// Inherited via Player
	virtual void strategy() override;
	virtual void interpretMessage(const std::string& message) override;

};

extern "C"
{
	__declspec(dllexport) IPlayer* getPlayer();
};


//create a PaperRockScissors instance
IPlayer* getPlayer()
{
	return &FILPlayer2::getInstance();
}
